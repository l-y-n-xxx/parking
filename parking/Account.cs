﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    public class Account
    {
        public Account()
        { }
        public Account(decimal b)
        {
            Deposit(b);
        }
        public decimal Balance { get; private set; }
        public void Deposit (decimal amount)
        {
            if (amount > 0)
                Balance += amount;
            else
                throw new ApplicationException($"You entered negative {amount} amount, which is not allowed!");
        }
        public void Withdraw(decimal amount)
        {
            if (amount > 0)
                Balance -= amount;// it's ok to put in minus
            else
                throw new ApplicationException($"You entered negative {amount} amount, which is not allowed!");
        }
    }

}
