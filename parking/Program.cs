﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace parking
{
    class Program
    {
        static Timer payCheckTimer = null, writeLogTimer = null; 
        static string legend = @"Welcome to Parking!
Please input number for corresponding action: 
1 - Current parking Balance
2 - Revenue for the last minute
3 - Free/Occupied number of slots
4 - All parking transactions for the last minute
5 - All parking transactions
6 - List all parked transport
7 - Park transport
8 - Take away transport
9 - Deposit transport balance
10 - Exit
and press Enter";
        static Parking parking = Parking.Instance;
        static bool keepWorking = true;

        static void Main(string[] args)
        {
            //for test
            //var culture = new System.Globalization.CultureInfo("ru-RU");
            //Thread.CurrentThread.CurrentCulture = culture;
            //Thread.CurrentThread.CurrentUICulture = culture;

            //Parking parking = Parking.Instance;
            Console.WriteLine(legend);
            Console.WriteLine();

            ReadSettings();
            payCheckTimer = new Timer( new TimerCallback(TickTimerPay), null, 1000, 1000);
            writeLogTimer = new Timer(new TimerCallback(TickTimerWriteLog), null, 60000, 60000);
            
            string userInput;
            while (keepWorking)
            {
                try
                {
                    Console.WriteLine();
                    Console.WriteLine("Please input number for next action or L for legend");
                    userInput = Console.ReadLine();
                    switch (userInput)
                    {
                        case "1":
                            Console.WriteLine($"Current parking balance = {parking.ActualBalance}$");
                            break;
                        case "2":
                            if (parking.TransactionsToShow.Count == 0)
                                Console.WriteLine("No revenue for the last minute");
                            else
                            {
                                Console.WriteLine($"Revenue for the last minute = {parking.TransactionsToShow.Select(s =>s.Amount).Sum().ToString("0.00")}$");
                            }
                            break;
                        case "3":
                            Console.WriteLine($"Total capacity = {parking.Capacity}: free = {parking.Capacity - parking.ParkedTransport.Count}, occupied = {parking.ParkedTransport.Count}");
                            break;
                        case "4":
                            if (parking.TransactionsToShow.Count == 0)
                                Console.WriteLine("Transaction list is empty");
                            else
                            {
                                Console.WriteLine("Here is the list of transactions:");
                                foreach (var t in parking.TransactionsToShow) //maybe draw a table
                                    Console.WriteLine($"id: {t.Identifier}, RegNum: {t.TransportRegNum}, Amount: {t.Amount.ToString("0.00")}, Time: {t.Time}");
                            }
                            break;
                        case "5":
                            ReadLogFile(Settings.TransactionLog);
                            break;
                        case "6":
                            if (parking.ParkedTransport.Count == 0)
                                Console.WriteLine("Parking is empty");
                            else
                            {
                                Console.WriteLine("Here is the list of parked transport:");
                                foreach (var t in parking.ParkedTransport) //maybe draw a table
                                    Console.WriteLine($"RegNum: {t.Item1.RegNum}, Type: {t.Item1.Type}");
                            }
                            break;
                        case "7":
                            Console.WriteLine("Please input your Registration number");
                            string rAdd = Console.ReadLine();

                            Console.WriteLine($"Please select your transport type: {string.Join(", ", parking.Tariffs.Keys)}");
                            string type = Console.ReadLine();
                            parking.CheckTariff(type);

                            Console.WriteLine("Please enter amount you want to deposit, if no, put 0");
                            decimal balInit;
                            while (!decimal.TryParse(Console.ReadLine(), out balInit))
                            {
                                Console.WriteLine("Please enter valid amount");
                            }

                            parking.AddTransport(rAdd, type, balInit);
                            Console.WriteLine($"Your transport has been parked, your tariff is {parking.CheckTariff(type)}$ per {parking.PaymentPeriod} seconds");
                            break;
                        case "8":
                            Console.WriteLine("Please input your Registration number");
                            parking.RemoveTransportByRegNum(Console.ReadLine());

                            Console.WriteLine($"You can take away your transport now\n"); 

                            break;
                        case "9":
                            Console.WriteLine("Please enter $ amount you want to deposit");
                            decimal bal;
                            while (!decimal.TryParse(Console.ReadLine(), out bal))
                            {
                                Console.WriteLine("Please enter valid amount");
                            }

                            Console.WriteLine("Please input your Registration number"); 
                            var r2 = Console.ReadLine();

                            parking.DepositTransportByRegNum(r2, bal);
                            Console.WriteLine($"Your balance is successfully deposited!");
                            break;
                        case "10":
                            keepWorking = false;
                            File.Delete(Settings.TransactionLog);
                            break;
                        case "L":
                            Console.WriteLine(legend);
                            break;
                        default:
                            Console.WriteLine("Unknown command, please press L, for legend");
                            break;
                    }
                    Console.WriteLine();
                }
                catch(FileNotFoundException fex)
                {
                    Console.WriteLine("Please wait for a minute, or press 4 to see the recent transactions");
                    Console.WriteLine();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }
            Console.WriteLine("Parking is closing, press any key");
            Console.ReadLine();
        }

        static void ReadSettings()
        {
            try
            {
                //ISettings s = new Settings();
                parking.Capacity = Settings.Capacity;

                if (Settings.Tariffs.Count == 0)
                    Console.WriteLine("tariffs are not defined!");
                else
                {
                    foreach (var key in Settings.Tariffs.AllKeys)
                        parking.Tariffs.Add(key, decimal.Parse(Settings.Tariffs[key], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture.NumberFormat));
                }
                parking.PaymentPeriod = Settings.PaymentPeriod;
                parking.Penalty = Settings.Penalty;

                parking.Deposit(Settings.InitialBalance);
            }
            catch (FormatException fex)
            {
                Console.WriteLine("ERROR - Check app.config file, dot (.) separator is expected for decimals");
                Console.WriteLine(fex.Message);
                keepWorking = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                keepWorking = false;
            }
        }

        static void TickTimerPay(object state)
        {
            TimeSpan span = new TimeSpan();
            DateTime time = new DateTime();
            if (parking.ParkedTransport.Count>0)
                foreach (var parked in parking.ParkedTransport)
                {
                    time = DateTime.Now;
                    span = time - parked.Item2;
                    if (span.Seconds % parking.PaymentPeriod == 0)
                        parking.MakeTransportPay(parked.Item1, time);
                }
        }

        static void TickTimerWriteLog(object state)
        {
            using (StreamWriter w = File.AppendText(Settings.TransactionLog))
            {
                List<Transaction> list = parking.Transactions.Where(t => !t.BackedUp).ToList();
                foreach (var transaction in list)
                {
                    w.WriteLine($"id:{transaction.Identifier}, regnum: {transaction.TransportRegNum}, amount:{transaction.Amount.ToString("0.00")}, time:{transaction.Time}");
                    transaction.BackedUp = true;
                }
                parking.Transactions.RemoveAll(t => t.BackedUp && !parking.TransactionsToShow.Contains(t));
            }
        }

        static void ReadLogFile(string filename)
        {           
            using (StreamReader file = new StreamReader(filename))
            {
                int counter = 0;
                string ln;

                while ((ln = file.ReadLine()) != null)
                {
                    Console.WriteLine(ln);
                    counter++;
                }
                file.Close();
                Console.WriteLine($"File has {counter} lines.");
            }
        }
    }
}
