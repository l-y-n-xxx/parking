﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;

namespace parking
{

    public static class Settings
    {
        [DefaultValue(10)]
        public static int Capacity
        {
            get
            {
                int c = int.TryParse(ConfigurationManager.AppSettings["Capacity"], out c)? c : 10;
                return c;
            }
        }

        [DefaultValue(0.00)]
        public static decimal InitialBalance
        {
            get
            {
                decimal b = decimal.TryParse(ConfigurationManager.AppSettings["InitialBalance"], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture.NumberFormat, out b) 
                    ? b : decimal.Parse("0.00",CultureInfo.InvariantCulture.NumberFormat);
                return b;
            }
        }

        [DefaultValue(5)]
        public static int PaymentPeriod
        {
            get
            {
                int p = int.TryParse(ConfigurationManager.AppSettings["PaymentPeriod"], out p) ? p : 5;
                return p;
            }
        }

        
        [DefaultValue(2.5)]
        public static decimal Penalty
        {
            get
            {
                decimal p = decimal.TryParse(ConfigurationManager.AppSettings["Penalty"], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture.NumberFormat, out p) 
                    ? p : decimal.Parse("2.5",CultureInfo.InvariantCulture.NumberFormat);
                return p;
            }
        }

        public static NameValueCollection Tariffs
        {
            get { return ConfigurationManager.GetSection("Tariffs") as NameValueCollection; }
        }

        public const string TransactionLog = "Transactions.log";
    }
}
