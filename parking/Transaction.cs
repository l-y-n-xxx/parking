﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    public class Transaction
    {
        public Transaction (string regNum, decimal amount, DateTime time)
        {
            Identifier = Guid.NewGuid();
            TransportRegNum = regNum;
            Amount = amount;
            Time = time;
        }

        public Guid Identifier { get; set; }
        public DateTime Time { get; set; }
        public string TransportRegNum { get; set; }
        public decimal Amount { get; set; }
        public bool BackedUp { get; set; }
    }
}
