﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    //public enum TransportType
    //{
    //    Car,
    //    Truck,
    //    Bus,
    //    Motocycle
    //}
    public class Transport
    {
        public Transport(string r, string t, decimal b)
        {
            RegNum = r;
            Type = t;
            TransportAccount = new Account(b);
        }
        public string RegNum { get; set; }
        public string Type { get; set; }

        private Account TransportAccount { get; set; }

        public decimal ActualBalance { get { return TransportAccount.Balance; } }
        public void Deposit(decimal amount)
        {
            TransportAccount.Deposit(amount);
        }

        public void Withdraw(decimal amount)
        {
            TransportAccount.Withdraw(amount);
        }
    }


}
